"use strict";

const recTime = 5;
let pwd = location.search || 'a'; pwd = pwd.trim().replace('?', '');

const video = document.querySelector("video"),
      butt  = document.querySelector("button");

let media, playFlag = false;

const play = async () => {
   try {
      let c = /Android|iPhone/i.test(navigator.userAgent) ?
         {video:{facingMode:{exact:"environment"}}, audio:true} :
         {video:true, audio:true};

      let stream = await navigator.mediaDevices.getUserMedia(c);
      video.srcObject = stream;
      video.play();

      media = new MediaRecorder(stream);
      media.ondataavailable = d => {
        var data = new FormData()
        data.append('file', d.data)

        fetch("/upload", {
            method: "POST",
            headers: {"X-PWD": pwd},
            body: data
        })
      };
      media.start(recTime * 1000);
   }
   catch(err) {alert(err);}
};

const go = () => {
   if (!playFlag) {
      butt.innerHTML = "&#9209;";
      play();
   }
   else {
      butt.innerHTML = "&#9210;";
      video.pause();
      video.srcObject = null;
      media.stop();
   }
   playFlag = !playFlag;
}