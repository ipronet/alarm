docker kill alarm_1
docker rm alarm_1
docker run \
	--restart=always -d \
	--name alarm_1 \
	-p 3002:3000 \
	 -v /root/alarm/uploads:/uploads \
	alarm:1.0.1
