const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const multer = require('multer')

const upload = multer({ dest: 'uploads/' })
const app = express();

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static('public'))

app.post('/upload', upload.single('file'), async (req, res) => {
    try {
        res.status(200).send({ status: 'ok' })
    } catch (err) {
        res.status(500).send(err)
    }
})

const port = process.env.PORT || 3000

app.listen(port, () =>
  console.log(`App is listening on port ${port}.`)
)

